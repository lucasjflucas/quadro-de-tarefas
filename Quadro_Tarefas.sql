Create schema `Tarefas`
use Tarefas;

CREATE TABLE Tarefas (
idtarefas INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
nome__tarefa VARCHAR(42) NOT NULL,
status_tarefa INT NOT NULL

);


CREATE TABLE Subtarefa (
idsubtarefa INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
Nome_sub VARCHAR(42) NOT NULL,
tarefas INT NOT NULL,
idstatus INT NOT NULL);


CREATE TABLE Status (
idstatus INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
descrição VARCHAR(42) NOT NULL);


CREATE TABLE Status_Tarefa (
id_status INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
Descrição VARCHAR(42) NOT NULL);


ALTER TABLE Tarefas ADD CONSTRAINT Tarefas_status_tarefa_Status_Tarefa_id_status FOREIGN KEY (status_tarefa) REFERENCES Status_Tarefa(id_status);
ALTER TABLE Subtarefa ADD CONSTRAINT Subtarefa_tarefas_Tarefas_idtarefas FOREIGN KEY (tarefas) REFERENCES Tarefas(idtarefas);
ALTER TABLE Subtarefa ADD CONSTRAINT Subtarefa_idstatus_Status_idstatus FOREIGN KEY (idstatus) REFERENCES Status(idstatus);
